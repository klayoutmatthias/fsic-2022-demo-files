
depth(5.0)
layers_file("xs.lyp")

trench = 3.0         # trench depth
sheet_bottom = 0.4   # sheet thickness
sheet_top = 0.8      # sheet thickness

l1 = layer("1/0")

# Idea:
# Use the inverse mask to grow a some sheet at the top
# before depositing the rest.

sub = bulk

sheet = mask(l1.inverted).grow(sheet_top - sheet_bottom, 0.0)

mask(l1).etch(trench, 0.0, into: sub)

sheet = sheet.or(deposit(sheet_bottom, sheet_bottom, mode: :round))

output("1/0", sub)
output("2/0", sheet)

