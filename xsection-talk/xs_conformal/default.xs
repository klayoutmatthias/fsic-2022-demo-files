
depth(5.0)
layers_file("xs.lyp")

trench = 3.0              # trench depth
sheet_thickness = 0.4     # sheet thickness

l1 = layer("1/0")

sub = bulk

mask(l1).etch(trench, 0.0, into: sub)

sheet = deposit(sheet_thickness, sheet_thickness, mode: :round)

output("1/0", sub)
output("2/0", sheet)

