
# -------------------------------------------------------------------
#  A custom device combiner (parallel only) for MOS caps

class MOSCAPNDeviceCombiner < RBA::GenericDeviceCombiner

  # A helper function checking whether two nets are the same one
  def same_net(a, b, name)
    na = a.net_for_terminal(name)
    nb = b.net_for_terminal(name)
    return na && nb && na.cluster_id == nb.cluster_id
  end
  
  # A helper function checking whether two parameters have the same value
  # (with some fuzzyness)
  def same_parameter(a, b, name)
    return (a.parameter(name) - b.parameter(name)).abs < 1e10
  end
  
  # This method is called by the system to check whether
  # devices can be combined and perform the combination in that
  # case.
  def combine_devices(a, b)

    # We only combine parallel MOS caps with the same W and L 
    # parameter. Combination accumulates "N".  
    if !same_net(a, b, "B")
      return false
    end
    if !same_net(a, b, "G")
      return false
    end
    if !same_net(a, b, "D")
      return false
    end
    if !same_parameter(a, b, "W")
      return false
    end
    if !same_parameter(a, b, "L")
      return false
    end

    # We have a combination candidate -> sum N values.
    a.set_parameter("N", a.parameter("N") + b.parameter("N"))
    
    # Disconnect the second device and let the system clean it up.
    b.disconnect_terminal("B")
    b.disconnect_terminal("G")
    b.disconnect_terminal("D")
    
    return true
  
  end

end

# -------------------------------------------------------------------
#  A custom device extractor for the MOS caps

class MOSCAPNExtractor < RBA::GenericDeviceExtractor

  def initialize(name)
    @name = name
  end

  # A helper method to setup up the device class for our MOSCAPN 
  # element.
  # Returns a new DeviceClass object configured for the MOSCAPN 
  
  def MOSCAPNExtractor.make_device_class(name)
  
    dc = RBA::DeviceClass::new
    dc.name = name
    dc.description = "MOS cap in n well"
    
    # Configures the device combiner
    dc.combiner = MOSCAPNDeviceCombiner::new
    
    # Sets device combination hints
    # NOTE: this is a hint for the extractor. It does not actually 
    # enable or disable device combination. The actual device combination
    # is done by the combiner.
    dc.supports_parallel_combination = true
    dc.supports_serial_combination = false
    
    # Declares the "G" (gate) terminal
    gate = RBA::DeviceTerminalDefinition::new("G", "Gate")
    dc.add_terminal(gate)
    
    # Declares the "D" (diffusion) terminal
    diffusion = RBA::DeviceTerminalDefinition::new("D", "Diffusion")
    dc.add_terminal(diffusion)
    
    # Declares the "B" (body) terminal
    bulk = RBA::DeviceTerminalDefinition::new("B", "Body")
    dc.add_terminal(bulk)
    
    # Declares the "W" parameter
    # NOTE: the parameters indicate: name "W", description "Width",
    # default value 0.0, is a primary parameter (i.e. compared), 
    # scaling factor 1e-6 (micron to metric units)
    dc.add_parameter(RBA::DeviceParameterDefinition::new("W", "Width", 0.0, true, 1e-6))
    
    # Defines the "L" parameter
    dc.add_parameter(RBA::DeviceParameterDefinition::new("L", "Length", 0.0, true, 1e-6))
  
    # Defines the "N" parameter
    dc.add_parameter(RBA::DeviceParameterDefinition::new("N", "Multiplier", 1, true, 1.0))
    
    return dc
  
  end

  # This method is called by the system to initialize the device
  
  def setup
  
    self.name = "MOSCAPN"
    
    # Create and register the device class
    dc = MOSCAPNExtractor.make_device_class(@name)
    self.register_device_class(dc)

    # Remember the terminal IDs for later
    @b_terminal_id = dc.terminal_id("B")  # -> 0 (first)
    @g_terminal_id = dc.terminal_id("G")  # -> 1
    @d_terminal_id = dc.terminal_id("D")  # -> 2

    # Sets up a layer for device recognition. 
    # This is supposed to be the overlap area where diffusion
    # and gate form the MOS cap.
    # Layer IDs are 0 for the first layer, 1 for the second etc.
    @a_layer_id = 0     # remember for later
    self.define_layer("A", "Cap area (for recognition)")

    # Sets up a layer for poly (gate). This layer is supposed
    # to extend beyond the recognition shape. The extension defines
    # the "L" axis.
    @p_layer_id = 1
    self.define_layer("P", "Poly")

    # Sets up a layer for diffusion (well). This layer is supposed
    # to extend beyond the recognition shape. The extension defines
    # the "W" axis.
    @d_layer_id = 2
    self.define_layer("D", "Diffusion")

    # Sets up a layer to output the poly terminals to
    @tp_layer_id = 3
    self.define_layer("tP", "Poly Terminal")

    # Sets up a layer to output the diffusion terminals to
    @td_layer_id = 4
    self.define_layer("tD", "Diffusion Terminal")

    # Sets up a layer to output the body terminal to.
    @tb_layer_id = 5
    self.define_layer("tB", "Body Terminal")

  end

  # This method is called by the system to get information about
  # how the layers are related.
  # NOTE: this is not an electrical, but a logical connectivity.
  # Using this information, the device extraction algorithm will
  # determine the clusters of shapes that are presented to 
  # "extract_devices".
  
  def get_connectivity(layout, layout_layers)
  
    a = layout_layers[@a_layer_id]
    p = layout_layers[@p_layer_id]
    d = layout_layers[@d_layer_id]
    
    conn = RBA::Connectivity::new
    
    # NOTE: in order to collect all connected shapes for the
    # recognition area (merged regions), we need to include a
    # self-connection for "A"
    conn.connect(a, a)
    
    # These connections will collect all poly and diffusion shapes
    # touching or overlapping "A"
    conn.connect(a, d)
    conn.connect(d, p)

  end

  # This method is called by the system to do the actual device
  # extraction. 
  
  def extract_devices(layer_geometry)
  
    (a, d, p) = layer_geometry
  
    # NOTE: "merged" will make sure that cap areas are merged
    # even if they are made from multiple shapes
      
    # Each merged "A" polygon is one potential device
    a.merged.each do |a_polygon|
    
      # The cap area needs to be a box
      if !a_polygon.is_box?
        self.error("Invalid shape of cap area", a_polygon)
        next
      end
    
      # Create a Region object from the device polygon as
      # Region objects are more convenient
      a_region = RBA::Region::new(a_polygon)
      
      # This will select all edges of "A" inside diffusion. 
      # These edges will define the "W" axis
      w_edges = a.edges.inside_part(d)
      if w_edges.count != 2
        self.error("Invalid diffusion overlap", a_polygon)
        next
      end
      
      # W is computed from the distance of the edges (they are
      # parallel as we selected boxes only)
      w = w_edges[0].distance_abs(w_edges[1].p1) * sdbu

      # Same for "A" edges inside poly. They will define "L".
            
      l_edges = a.edges.inside_part(p)
      if l_edges.count != 2
        self.error("Invalid poly overlap", a_polygon)
        next
      end
      
      l = l_edges[0].distance_abs(l_edges[1].p1) * sdbu
      
      # Make the device. "create_device" is a built-in method which
      # generates and pre-configures a device from the device class
      # we have given by "register_device_class".
      device = self.create_device

      # Provide a transformation that centers the device inside it's cell 
      # (not required, but better for generating layout from netlists)
      pos = a_polygon.bbox.center
      device.trans = RBA::DCplxTrans::new(RBA::DVector::new(pos.x * self.dbu, pos.y * self.dbu))

      # Configures the parameters we just computed.
      device.set_parameter("W", w)
      device.set_parameter("L", l)
      
      # And supply terminal shapes. These shapes are added to the 
      # respective layers and establish the connection between the 
      # device terminals and the conductive regions on these layers.
      self.define_terminal(device, @g_terminal_id, @tp_layer_id, a_polygon)
      self.define_terminal(device, @d_terminal_id, @td_layer_id, a_polygon)
      self.define_terminal(device, @b_terminal_id, @tb_layer_id, a_polygon)
      
    end
  
  end
  
end

# This is a convenience method similar to "mos4".
# It provides the device extractor object from above.

def moscapn(name)
  MOSCAPNExtractor::new(name)
end
