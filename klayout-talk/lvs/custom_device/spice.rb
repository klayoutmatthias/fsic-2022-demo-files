
# -------------------------------------------------------------------
# A custom Spice reader delegate which implements subcircuit mapping
# of devices.
#
# "subcircuit mapping" means that a device is not written as a
# standard Spice element (like "R", "M" oder "C"), but instead 
# a subcircuit is instantiated. This subcircuit typically contains
# a complex device model, is put into a separate include file
# and is parametrized.
#
# This is a generic implementation which is based on the 
# ability of DeviceClass to provide terminal and parameter
# definitions. The only customization is the list of device classes
# we are generating in the constructor.

class SubcircuitModelsReaderDelegate < RBA::NetlistSpiceReaderDelegate

  # Initialization: create a number of device classes for us
  # to use later.
  def initialize
    @device_classes = [
      MOSCAPNExtractor::make_device_class("MOSCAPN")
      # TODO: you might want to add more here
    ]
  end

  # Implements the delegate interface:
  # Says we want to catch these subcircuits as devices.
  # This implementation captures subcircuits if their name
  # matches one of the devices we provide.
  def wants_subcircuit(name)
    @device_classes.collect { |c| c.name }.member?(name)
  end

  # This method is called by the system before the reading starts. 
  # We use this opportunity to install the device classes for our
  # custom devices.
  def start(netlist)
    @device_classes.each do |c|
      netlist.add(c)
    end
  end

  # Implements the delegate interface: 
  # Takes and translate the element
  def element(circuit, el, name, model, value, nets, params)

    if el != "X"
      # All other elements are left to the standard implementation
      return super
    end

    # Gets the device class
    cls = circuit.netlist.device_class_by_name(model)
    if ! cls
      # Unknown model -> this should actually not happen as we selected
      # only subcircuits from known device classes.
      return super
    end

    # Create the device from the class we derived
    device = circuit.create_device(cls, name)

    # Checks the number of nets vs. number of terminals
    
    td = cls.terminal_definitions
    if nets.size != td.size
      error("Subcircuit #{model} needs #{td.size} node(s)")
    end

    # Configures the device.
    # NOTE: this is a generic approach which uses the definitions
    # from the device classes. This means the terminals in the 
    # class are mapped to the subcircuit pins in the order they
    # are defined there.
    
    td.each_with_index do |t,index|
      device.connect_terminal(index, nets[index])
    end

    cls.parameter_definitions.each do |p|
      device.set_parameter(p.name, (params[p.name] || 0.0) / p.si_scaling)
    end

    return true

  end

end

# A convinience function that creates a customized Spice reader for
# use with "schematic" in the LVS script.
def custom_spice_reader
  # Instantiate a reader using the new delegate
  reader = RBA::NetlistSpiceReader::new(SubcircuitModelsReaderDelegate::new)
end


# -------------------------------------------------------------------
# The corresponding Spice writer delegate

class SubcircuitModelsWriterDelegate < RBA::NetlistSpiceWriterDelegate

  # Constructor
  def initialize
    # create a list of device names for which to generate
    # subcircuits
    @models = [ 
      "MOSCAPN"
      # TODO: you might want to add more here
    ]
  end

  # This method is called by the system at the beginning to generate
  # the header
  def write_header
    # TODO: change name of file
    emit_line(".INCLUDE 'models.cir'")
  end
  
  # A utility method for format the parameter in Spice fashion
  def format_parameter(pd, device)
    value = device.parameter(pd.id) * pd.si_scaling
    if value <= 1e-10
      return pd.name + ("=%.12gp" % (value * 1e12))
    elsif value <= 1e-7
      return pd.name + ("=%.12gn" % (value * 1e9))
    elsif value <= 1e-4
      return pd.name + ("=%.12gu" % (value * 1e6))
    elsif value <= 1e-1
      return pd.name + ("=%.12gm" % (value * 1e3))
    elsif value <= 1e2
      return pd.name + ("=%.12g" % (value))
    elsif value <= 1e5
      return pd.name + ("=%.12gk" % (value * 1e-3))
    elsif value <= 1e8
      return pd.name + ("=%.12gmeg" % (value * 1e-6))
    else
      return pd.name + ("=%.12g" % value)
    end
  end
  
  # This method is called by the system to write a specific device
  def write_device(device)
  
    device_class = device.device_class
    if @models.member?(device_class.name)
    
      # build a Spice card for the subcircuit call
      str = "X" + device.expanded_name
      device_class.terminal_definitions.each do |td|
        str += " " + net_to_string(device.net_for_terminal(td.id))
      end
      str += " " + device_class.name
      str += " PARAMS:"
      device_class.parameter_definitions.each do |pd|
        str += " " + format_parameter(pd, device)
      end
      emit_line(str)
      
    else
      # other devices use the standard implementation
      super
    end
      
  end      

end

# A convinience function that creates a customized Spice reader for
# use with "schematic" in the LVS script.
def custom_spice_writer
  writer = RBA::NetlistSpiceWriter::new(SubcircuitModelsWriterDelegate::new)
  writer.use_net_names= true
  writer.with_comments = false
  writer
end

