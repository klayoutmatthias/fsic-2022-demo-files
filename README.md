# FSiC 2022 demo files

Demo files for my presentations @ FSiC 2022 conference in Paris

FSiC-2022 conference page: (https://wiki.f-si.org/index.php/FSiC2022)


## "KLayout XSection tool - Deep insights or nonsense in colors?" 

[Abstract and presentation](https://wiki.f-si.org/index.php?title=KLayout_XSection_tool_-_Deep_insights_or_nonsense_in_colors%3F)

Files in "xsection-talk" subfolder

## "Tutorial and FAQ on physical verification, DRC+LVS" 

[Abstract and presentation](https://wiki.f-si.org/index.php?title=Tutorial_and_FAQ_on_physical_verification,_DRC%2BLVS")

Files in "klayout-talk" subfolder

